
Module: FERank
Authors: Amauri CHAMPEAUX


Description
===========
Add the audience measurement tool FERank on all pages of the site.

Requirements
============
1. Register for free on https://www.ferank.fr/inscription/

Installation
============
1. Unpack the 'ferank' folder and contents in the appropriate modules
   directory of your Drupal installation.  This is probably
   'sites/all/modules/'

2. Enable the FERank module in the administration tools,
   it is classified in "Statistics".

3. Visit the FERank settings page and make appropriate configurations
   'Home > Administration > System > FERank'
